import numpy as np
arr = np.array([[1,2,3,4],[4,2,6]])
print(arr.ndim)
print("Number of dimensions it has",arr.ndim)
print ("number of shapes it has",arr.shape)
print("Data type of it's",arr.dtype)


# array creation
# creating array from list with type float

a =  np.array([[1,2,4],[5,8,7]],dtype="float")
print("array created using passed list:\n",a)
print("type of a is", a.dtype)

# creating an array from a touple
b = np.array((1,2,3,4,5,6))
print("touple of array will be",b)

#constant value of complex array
d = np.full((3,3),6,dtype="complex")
print("\n an array initialized with all 6s.""array type is complex:\n",d)

# creating a constant value array of complex type 

c = np.zeros((3,4))
print("\nan array initialized with all zeros ",c)